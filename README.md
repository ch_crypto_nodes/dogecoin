# Dogecoin

A Dogecoin docker image.

## Tags

- `1.14.5`
- `1.14.3`

NOTE: [releases](https://github.com/dogecoin/dogecoin/releases)

## What is Dogecoin?

Dogecoin is a cryptocurrency like Bitcoin, although it does not use SHA256 as its proof of work (POW). Taking 
development cues from Tenebrix and Litecoin, Dogecoin currently employs a simplified variant of scrypt.   
Learn more about Dogecoin on the [official Dogecoin website](https://dogecoin.com/).


## Usage

### How to use this image

You must run a container with predefined environment variables.

Create `.env` file with following variables:

| Env variable                    | Description                                                                |
| ------------------------------- | -------------------------------------------------------------------------- |
| `RPC_USER_NAME`                 | User to secure the JSON-RPC api.                                           |
| `RPC_USER_PASSWORD`             | A password to secure the JSON-RPC api.                                     | 
| `PATH_TO_DATA_FOLDER`           | Full path to the blockchain data folder.                                   |

Run container: 

```bash
docker-compose up -d
```

Stop container: 

```bash
docker-compose down 
```
